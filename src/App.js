import React, { Component } from 'react';
import { Grid, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Glyphicon, InputGroup } from 'react-bootstrap';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: 'Manikandan',
      law: '',
      lawName: ''
    };
  }

  letterCount = (string, letter) => {
      let count = 0;
      string = string.toUpperCase();
      letter = letter.toUpperCase();
      for (let i=0; i<string.length; i += 1) {
          if (string[i] === letter) {
              count += 1;
          }
      }
      return count;
  };

  handleChange = event => {
    let name = event.target.value;
    this.setState({name});
  };

  handleSubmit = event => {
    let fullName = this.state.name;
    let lawName = '';
    let htmlName = [];
    for (let i = 0; i < fullName.length; i++) {
        let currentChar = fullName[i];
        let currentValue = fullName.slice(i);
        let counter = this.letterCount(currentValue, currentChar);
        htmlName.push(<React.Fragment key={i}>{currentChar}<sup>{counter}</sup> </React.Fragment>);
        if(counter === 1) {
          lawName += currentChar;
        }
    }

    this.setState({law: htmlName, lawName});

    event.preventDefault();
  };

  render() {
    return (
      <div className="container">
          <div className="row">
            <div className="col-md-5">
              <h2>Welcome to Mikdan's Law</h2>
              <Form onSubmit={this.handleSubmit} inline>
              <FormGroup>
                <InputGroup>
                  <InputGroup.Addon>
                    <Glyphicon glyph="user" />
                  </InputGroup.Addon>
                  <FormControl type="text" placeholder="Enter your name" value={this.state.name} onChange={this.handleChange} />
                  <InputGroup.Button>
                  <Button type="submit" bsStyle="success">Show</Button>
                  <Button type="reset" bsStyle="danger">Clear</Button>
                  </InputGroup.Button>
                </InputGroup>
              </FormGroup>
              </Form>
              <br/>
              <div className="well well-sm"><h2>{this.state.name}</h2></div>
              <br/>
              <div className="well well-sm"><h2>{this.state.law}</h2></div>
              <br/>
              <div className="well well-sm"><h2>{this.state.lawName}</h2></div>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-xs-5">
              <footer>
                <p>&copy; Copyright 2019 Mikdan Tech Solutions</p>
              </footer>
            </div>
          </div>
        </div>
    );
  }
}

export default App;
